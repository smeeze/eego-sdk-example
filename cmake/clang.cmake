if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fvisibility=hidden -fvisibility-inlines-hidden")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fdata-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -ffunction-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,--gc-sections")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wl,-s")

  add_library(warnings_high INTERFACE)
  add_library(warnings::high ALIAS warnings_high)
  target_compile_options(warnings_high INTERFACE 
    -Werror=all;-Werror=extra;-Werror=non-virtual-dtor;-Werror=reorder;-Werror=unused-result;-Wno-error=missing-braces;-Wno-error=format-security
  )

  set(LIBRARY_GROUP_START "-Wl,--start-group")
  set(LIBRARY_GROUP_END "-Wl,--end-group")
endif() # Clang
