if(WIN32)
  add_definitions(/wd4251)
# add_definitions(/wd4996)
  add_definitions(/wd5105)
  add_definitions(/D__PRETTY_FUNCTION__=__FUNCSIG__)
  add_definitions(/D_USE_MATH_DEFINES)
  add_definitions(/permissive-)
  add_definitions(/Zc:preprocessor)

  add_library(warnings_high INTERFACE)
  add_library(warnings::high ALIAS warnings_high)
  target_compile_options(warnings_high INTERFACE
    /W3
#   /WX
  )
endif() # WIN32
