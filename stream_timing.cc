// system
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
// self
#include <eemagine/sdk/helper.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
void test_amplifier(const std::unique_ptr<eemagine::sdk::amplifier> &amplifier,
                    int rate, int delay_ms) {
  eemagine::sdk::helper::print(amplifier);

  std::unique_ptr<eemagine::sdk::stream> stream(amplifier->OpenEegStream(rate));
  const auto s0(std::chrono::high_resolution_clock::now());
  auto stamp_delay(s0);
  auto stamp_end(s0 + std::chrono::milliseconds(1000));
  while (stamp_delay < stamp_end) {
    stamp_delay += std::chrono::milliseconds(delay_ms);
    std::this_thread::sleep_until(stamp_delay);

    const auto s1(std::chrono::high_resolution_clock::now());
    const auto d(stream->getData());
    const auto s2(std::chrono::high_resolution_clock::now());
    const auto d0(
        std::chrono::duration_cast<std::chrono::microseconds>(s1 - s0).count());
    const auto d1(
        std::chrono::duration_cast<std::chrono::microseconds>(s2 - s1).count());
    std::cout << "d0=" << d0 << "us d1=" << d1 << "us " << d.getChannelCount()
              << "x" << d.getSampleCount() << "\n";
  }
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    eemagine::sdk::helper helper;

    const auto version(helper.getVersion());
    eemagine::sdk::helper::print(version);

    // move pointers provided by factory into unique_ptr's
    std::vector<std::unique_ptr<eemagine::sdk::amplifier>> amplifiers;
    for (auto amplifier : helper.getAmplifiers()) {
      amplifiers.push_back(
          std::unique_ptr<eemagine::sdk::amplifier>(amplifier));
    }
    // for each amplifier, run test
    for (const auto &amplifier : amplifiers) {
      test_amplifier(amplifier, 512, 1000 / 10);
    }
  } catch (const std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return -1;
  }
  return 0;
}
