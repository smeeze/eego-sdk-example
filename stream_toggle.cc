// system
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
// self
#include <eemagine/sdk/helper.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
void test_amplifier(const std::unique_ptr<eemagine::sdk::amplifier> &amplifier,
                    int rate) {
  eemagine::sdk::helper::print(amplifier);

  for (int n = 0; n < 20; ++n) {
    std::unique_ptr<eemagine::sdk::stream> stream;
    if (n % 2) {
      std::cout << "impedance...\n";
      stream.reset(amplifier->OpenImpedanceStream());
    } else {
      std::cout << "EEG...\n";
      stream.reset(amplifier->OpenEegStream(rate));
    }
    const auto s0(std::chrono::high_resolution_clock::now());
    auto d(s0);
    for (int i = 0; i < 20; ++i) {
      d += std::chrono::milliseconds(1000 / 20);
      std::this_thread::sleep_until(d);

      const auto s1(std::chrono::high_resolution_clock::now());
      const auto d(stream->getData());
      const auto s2(std::chrono::high_resolution_clock::now());
      const auto d0(
          std::chrono::duration_cast<std::chrono::milliseconds>(s1 - s0)
              .count());
      const auto d1(
          std::chrono::duration_cast<std::chrono::milliseconds>(s2 - s1)
              .count());
      std::cout << "n=" << n << " i=" << i << " d0=" << d0 << "ms d1=" << d1
                << "ms " << d.getChannelCount() << "x" << d.getSampleCount()
                << "\n";
    }
  }
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    eemagine::sdk::helper helper;

    const auto version(helper.getVersion());
    eemagine::sdk::helper::print(version);

    // move pointers provided by factory into unique_ptr's
    std::vector<std::unique_ptr<eemagine::sdk::amplifier>> amplifiers;
    for (auto amplifier : helper.getAmplifiers()) {
      amplifiers.push_back(
          std::unique_ptr<eemagine::sdk::amplifier>(amplifier));
    }
    // for each amplifier
    for (const auto &amplifier : amplifiers) {
      test_amplifier(amplifier, 512);
    }
  } catch (const std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return -1;
  }
  return 0;
}
