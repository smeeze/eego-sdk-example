#pragma once

// system
#include <memory>
// eemagine sdk
#include <eemagine/sdk/factory.h>

///////////////////////////////////////////////////////////////////////////////
namespace eemagine {
namespace sdk {
class helper {
public:
  helper();

  auto getVersion() const { return _factory.getVersion(); }
  auto getAmplifier() { return _factory.getAmplifier(); }
  auto getAmplifiers() { return _factory.getAmplifiers(); }

  static void print(const eemagine::sdk::factory::version &);

  static void print(const eemagine::sdk::amplifier *);
  static void
  print(const std::unique_ptr<eemagine::sdk::amplifier> &amplifier) {
    print(amplifier.get());
  }
  static void
  print(const std::shared_ptr<eemagine::sdk::amplifier> &amplifier) {
    print(amplifier.get());
  }

  static void print(const eemagine::sdk::stream *);
  static void print(const std::unique_ptr<eemagine::sdk::stream> &stream) {
    print(stream.get());
  }
  static void print(const std::shared_ptr<eemagine::sdk::stream> &stream) {
    print(stream.get());
  }

  static void print(const eemagine::sdk::buffer &);

protected:
  eemagine::sdk::factory _factory;
};
} // namespace sdk
} // namespace eemagine
