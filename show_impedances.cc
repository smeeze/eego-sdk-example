// system
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
// self
#include <eemagine/sdk/helper.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    eemagine::sdk::helper factory;

    //
    // show version
    //
    const auto version(factory.getVersion());
    eemagine::sdk::helper::print(version);

    //
    // get amplifiers
    //
    auto amps(factory.getAmplifiers());
    std::vector<std::shared_ptr<eemagine::sdk::amplifier>> amps_mg;
    for (auto *a : amps) {
      amps_mg.emplace_back(std::shared_ptr<eemagine::sdk::amplifier>(a));
    }
    for (const auto &amplifier : amps_mg) {
      std::cout << "amplifier:\n";
      std::cout << "  type...... " << amplifier->getType() << "\n";
      std::cout << "  version... " << amplifier->getFirmwareVersion() << "\n";
      std::cout << "  serial.... " << amplifier->getSerialNumber() << "\n";

      //
      // open stream
      //
      std::unique_ptr<eemagine::sdk::stream> stream(
          amplifier->OpenImpedanceStream());
      if (stream == nullptr) {
        throw(std::runtime_error("could not open impedance stream"));
      }

      //
      // get stream channel indices
      //
      int imp_gnd_idx(-1);
      int imp_ref_idx(-1);
      for (const auto &channel : stream->getChannelList()) {
        switch (channel.getType()) {
        case eemagine::sdk::channel::impedance_reference:
          imp_ref_idx = channel.getIndex();
          break;
        case eemagine::sdk::channel::impedance_ground:
          imp_gnd_idx = channel.getIndex();
          break;
        default:
          break;
        }
      }
      if (imp_ref_idx == -1) {
        throw(std::runtime_error("reference channel not found in stream"));
      }
      if (imp_gnd_idx == -1) {
        throw(std::runtime_error("ground channel not found in stream"));
      }

      //
      // show impedance data for a couple of seconds
      //
      for (int n = 0; n < 10; ++n) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        const auto buffer(stream->getData());
        if (buffer.getSampleCount() == 0) {
          std::cout << "no data\n";
        } else {
          int nl_check(0);
          std::cout << "impedances:";
          for (int chan_idx = 0; chan_idx < buffer.getChannelCount() - 2;
               ++chan_idx) {
            if (nl_check == 8) {
              nl_check = 0;
              std::cout << "\n          :";
            }
            std::cout << " " << (double)buffer.getSample(chan_idx, 0) / 1000
                      << "k";
            ++nl_check;
          }
          std::cout << "\n";
          std::cout << "       ref: "
                    << (double)buffer.getSample(imp_ref_idx, 0) / 1000 << "k\n";
          std::cout << "       ref: "
                    << (double)buffer.getSample(imp_gnd_idx, 0) / 1000 << "k\n";
        }
        std::cout << "\n";
      }
    }
  } catch (const std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return -1;
  }
  return 0;
}
