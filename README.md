# eego-SDK examples

example project for the eego-SDK.

## requirements
* C++ compiler that supports C++11
* cmake 3.11
* git
* eego-SDK

## supported platforms
* windows(32 and 64 bit)
* linux(32 and 64 bit)

## setup
create a user.cmake file in the root of this project. Add a line pointing
to the eego-SDK zip file, like so:
```
set(EEGO_SDK_ZIP /path/to/download/eego-sdk-1.3.19.40453.zip)
```

## build
build using standard cmake. i.e. create build directory and
from there, call cmake:
```
cmake -DCMAKE_BUILD_TYPE=Release /path/to/this_project && cmake --build .
```
